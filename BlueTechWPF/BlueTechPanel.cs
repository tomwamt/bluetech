﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace BlueTech.Windows
{
    public class BlueTechPanel : Canvas
    {
        private Dictionary<string, ImageSource> images = new Dictionary<string, ImageSource>();
        private Point lastPoint;

        public FunctionBlock ViewedBlock
        {
            get { return (FunctionBlock)GetValue(ViewedBlockProperty); }
            set { SetValue(ViewedBlockProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ViewedBlock.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ViewedBlockProperty =
            DependencyProperty.Register("ViewedBlock", typeof(FunctionBlock), typeof(BlueTechPanel), new FrameworkPropertyMetadata(null));

        public Location SelectedLocation
        {
            get { return (Location)GetValue(SelectedLocationProperty); }
            set { SetValue(SelectedLocationProperty, value); }
        }

        // Using a DependencyProperty as the backing store for SelectedLocation.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SelectedLocationProperty =
            DependencyProperty.Register("SelectedLocation", typeof(Location), typeof(BlueTechPanel), new FrameworkPropertyMetadata(new Location(), FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        public Location SelectionStart
        {
            get { return (Location)GetValue(SelectionStartProperty); }
            set { SetValue(SelectionStartProperty, value); }
        }

        // Using a DependencyProperty as the backing store for SelectionStart.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SelectionStartProperty =
            DependencyProperty.Register("SelectionStart", typeof(Location), typeof(BlueTechPanel), new FrameworkPropertyMetadata(new Location(), FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        //public Dictionary<Location, Block> Clipboard { get; set; } = new Dictionary<Location, Block>();

        public int BlockSize { get; set; } = 32;
        public double OffsetX { get; set; }
        public double OffsetY { get; set; }

        public BlueTechPanel()
        {
            LoadImage("background");
            LoadImage("border_up");
            LoadImage("border_up_in");
            LoadImage("border_up_out");
            LoadImage("border_down");
            LoadImage("border_down_in");
            LoadImage("border_down_out");
            LoadImage("border_left");
            LoadImage("border_left_in");
            LoadImage("border_left_out");
            LoadImage("border_right");
            LoadImage("border_right_in");
            LoadImage("border_right_out");

            LoadImage("wire_horizontal");
            LoadImage("wire_vertical");
            LoadImage("wire_turn_up_left");
            LoadImage("wire_turn_up_right");
            LoadImage("wire_turn_down_left");
            LoadImage("wire_turn_down_right");
            LoadImage("wire_t_up");
            LoadImage("wire_t_down");
            LoadImage("wire_t_left");
            LoadImage("wire_t_right");
            LoadImage("wire_cross");

            LoadImage("select");

            LoadImage("input");
            LoadImage("output");

            LoadImage("delay");
            LoadImage("invert");

            LoadImage("mask");

            LoadImage("shift_left");
            LoadImage("shift_right");

            LoadImage("function");

            MouseDown += HandleMouseDown;
            MouseUp += HandleMouseUp;
            MouseMove += HandleMouseMoved;
        }

        private void HandleMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Right)
                lastPoint = e.GetPosition(this);

            if (e.ChangedButton == MouseButton.Left)
            {
                SelectionStart = ScreenToWorldSpace(e.GetPosition(this));
                SelectedLocation = ScreenToWorldSpace(e.GetPosition(this));

                InvalidateVisual();
            }
        }

        private void HandleMouseUp(object sender, MouseButtonEventArgs e)
        {
            if(e.ChangedButton == MouseButton.Left)
            {
                InvalidateVisual();
            }
        }

        private void HandleMouseMoved(object sender, MouseEventArgs e)
        {
            if(e.RightButton == MouseButtonState.Pressed)
            {
                Point p = e.GetPosition(this);
                Vector d = p - lastPoint;
                lastPoint = p;

                OffsetX += d.X;
                OffsetY += d.Y;

                InvalidateVisual();
            }

            if (e.LeftButton == MouseButtonState.Pressed)
            {
                SelectedLocation = ScreenToWorldSpace(e.GetPosition(this));
                InvalidateVisual();
            }
        }

        private void LoadImage(string name)
        {
            BitmapImage img = new BitmapImage(new Uri($@"pack://application:,,,/BlueTechWPF;component/Images/{name}.png", UriKind.Absolute));
            images[name] = img;
        }

        protected override void OnRender(DrawingContext drawingContext)
        {
            base.OnRender(drawingContext);

            if(ViewedBlock != null)
            {
                foreach (KeyValuePair<Location, Block> kvp in ViewedBlock.GetBlocks())
                {
                    Point p = WorldToScreenSpace(kvp.Key);
                    DrawBlock(drawingContext, kvp.Value, p, BlockSize);
                }

                Point selP = WorldToScreenSpace(SelectedLocation);
                Rect rect = new Rect(selP, new Size(BlockSize, BlockSize));
                drawingContext.DrawImage(images["select"], rect);

                Point selBoxP = WorldToScreenSpace(SelectionStart);
                Rect bigRect = new Rect(Math.Min(selBoxP.X, selP.X), Math.Min(selBoxP.Y, selP.Y),
                    (Math.Abs(SelectionStart.X - SelectedLocation.X) + 1) * BlockSize, (Math.Abs(SelectionStart.Y - SelectedLocation.Y) + 1) * BlockSize);
                drawingContext.DrawRectangle(new SolidColorBrush { Color = Colors.Red, Opacity = 0.1 }, new Pen(Brushes.Red, 1), bigRect);
            }
        }

        private Point WorldToScreenSpace(Location loc)
        {
            int x = (int)(ActualWidth / 2 + (loc.X * BlockSize) + OffsetX);
            int y = (int)(ActualHeight / 2 - (loc.Y * BlockSize) + OffsetY);
            return new Point(x, y);
        }

        private Location ScreenToWorldSpace(Point p)
        {
            int x = (int)Math.Floor((p.X - ActualWidth / 2 - OffsetX) / BlockSize);
            int y = (int)Math.Ceiling((ActualHeight / 2 + OffsetY - p.Y) / BlockSize);
            return new Location(x, y);
        }

        private void DrawBlock(DrawingContext dc, Block block, Point p, int size)
        {
            Rect rect = new Rect(p, new Size(size, size));
            if(block is Wire)
            {
                switch (((Wire)block).Format)
                {
                    case WireFormat.CROSS:
                    case WireFormat.BRIDGE:
                        dc.DrawImage(images["wire_cross"], rect);
                        break;
                    case WireFormat.HORIZONTAL:
                        dc.DrawImage(images["wire_horizontal"], rect);
                        break;
                    case WireFormat.VERTICAL:
                        dc.DrawImage(images["wire_vertical"], rect);
                        break;
                    case WireFormat.TURN_UP_LEFT:
                        dc.DrawImage(images["wire_turn_up_left"], rect);
                        break;
                    case WireFormat.TURN_UP_RIGHT:
                        dc.DrawImage(images["wire_turn_up_right"], rect);
                        break;
                    case WireFormat.TURN_DOWN_LEFT:
                        dc.DrawImage(images["wire_turn_down_left"], rect);
                        break;
                    case WireFormat.TURN_DOWN_RIGHT:
                        dc.DrawImage(images["wire_turn_down_right"], rect);
                        break;
                    case WireFormat.T_UP:
                        dc.DrawImage(images["wire_t_up"], rect);
                        break;
                    case WireFormat.T_DOWN:
                        dc.DrawImage(images["wire_t_down"], rect);
                        break;
                    case WireFormat.T_LEFT:
                        dc.DrawImage(images["wire_t_left"], rect);
                        break;
                    case WireFormat.T_RIGHT:
                        dc.DrawImage(images["wire_t_right"], rect);
                        break;
                }
            }
            else
            {
                dc.DrawImage(images["background"], rect);

                if (block.IsInput(BlockSide.UP))
                    dc.DrawImage(images["border_up_in"], rect);
                else if (block.IsOutput(BlockSide.UP))
                    dc.DrawImage(images["border_up_out"], rect);
                else
                    dc.DrawImage(images["border_up"], rect);

                if (block.IsInput(BlockSide.DOWN))
                    dc.DrawImage(images["border_down_in"], rect);
                else if (block.IsOutput(BlockSide.DOWN))
                    dc.DrawImage(images["border_down_out"], rect);
                else
                    dc.DrawImage(images["border_down"], rect);

                if (block.IsInput(BlockSide.LEFT))
                    dc.DrawImage(images["border_left_in"], rect);
                else if (block.IsOutput(BlockSide.LEFT))
                    dc.DrawImage(images["border_left_out"], rect);
                else
                    dc.DrawImage(images["border_left"], rect);

                if (block.IsInput(BlockSide.RIGHT))
                    dc.DrawImage(images["border_right_in"], rect);
                else if (block.IsOutput(BlockSide.RIGHT))
                    dc.DrawImage(images["border_right_out"], rect);
                else
                    dc.DrawImage(images["border_right"], rect);

                if(block is DelayInverter)
                {
                    if (((DelayInverter)block).Invert)
                        dc.DrawImage(images["invert"], rect);
                    else
                        dc.DrawImage(images["delay"], rect);
                }
                else if(block is Mask)
                {
                    dc.DrawImage(images["mask"], rect);
                }
                else if(block is Shifter)
                {
                    if (((Shifter)block).ShiftAmount > 0)
                        dc.DrawImage(images["shift_left"], rect);
                    else
                        dc.DrawImage(images["shift_right"], rect);
                }
                else if(block is InputBlock)
                {
                    dc.DrawImage(images["input"], rect);
                }
                else if(block is OutputBlock)
                {
                    dc.DrawImage(images["output"], rect);
                }
                else if(block is FunctionBlock)
                {
                    dc.DrawImage(images["function"], rect);
                }
            }
        }

        public void HandleViewedBlockChanged(object sender, EventArgs e)
        {
            InvalidateVisual();
        }
    }
}
