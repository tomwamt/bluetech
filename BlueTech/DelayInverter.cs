﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlueTech
{
    public class DelayInverter : Block, ISingleInBlock, ISingleOutBlock
    {
        private List<ushort> valueQueue;
        private bool firstTick = true;

        public int Delay { get; } = 1; // TODO set
        public bool Invert { get; set; } = false;
        public BlockSide InputSide { get; set; } = BlockSide.DOWN;
        public BlockSide OutputSide { get; set; } = BlockSide.UP;

        public DelayInverter()
        {
            valueQueue = new List<ushort>(new ushort[Delay + 1]);
        }

        public override ushort GetValues(BlockSide side)
        {
            if (Invert) return (ushort)(~valueQueue[0]);
            else return valueQueue[0];
        }

        public override void HandleBlockUpdate(object sender, BlockUpdateEventArgs e)
        {
            //Block block = sender as Block;
            //if(block != null)
            //{
            //    valueQueue[Delay] = block.GetValues(InputSide.Opposite);
            //}
        }

        public override void Tick()
        {
            ushort oldValue = valueQueue[0];
            valueQueue.RemoveAt(0);
            //ushort last = valueQueue.Last();
            //valueQueue.Add(last);
            if(valueQueue[0] != oldValue || firstTick)
                OnBlockUpdate(new BlockUpdateEventArgs());
            firstTick = false;
            valueQueue.Add(Neighbors[InputSide].GetValues(InputSide.Opposite));
        }

        public override bool IsInput(BlockSide side)
        {
            return side == InputSide;
        }

        public override bool IsOutput(BlockSide side)
        {
            return side == OutputSide;
        }

        public override Block Clone()
        {
            DelayInverter block = (DelayInverter)MemberwiseClone();
            block.valueQueue = new List<ushort>(new ushort[Delay + 1]);
            return block;
        }
    }
}
