﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlueTech
{
    public class InputBlock : Block, ISingleOutBlock
    {
        private ushort values;

        public BlockSide OutputSide { get; set; }

        public void SetValues(ushort values, bool forceUpdate=false)
        {
            ushort oldValues = this.values;
            this.values = values;
            if(forceUpdate | oldValues != values)
                OnBlockUpdate(new BlockUpdateEventArgs());
        }

        public override ushort GetValues(BlockSide side)
        {
            if (IsOutput(side))
                return values;
            else
                return 0;
        }

        public override bool IsInput(BlockSide side)
        {
            return false;
        }

        public override bool IsOutput(BlockSide side)
        {
            return side == OutputSide;
        }

        public override void HandleBlockUpdate(object sender, BlockUpdateEventArgs e){ }

        public override Block Clone()
        {
            return (Block)MemberwiseClone();
        }
    }
}
