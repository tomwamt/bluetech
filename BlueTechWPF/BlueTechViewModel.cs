﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;

namespace BlueTech.Windows
{
    public class BlueTechViewModel : INotifyPropertyChanged
    {
        #region private fields
        private Dictionary<Location, Block> clipboard = new Dictionary<Location, Block>();
        #endregion

        #region properties
        public IEnumerable<WireFormat> WireFormats
        {
            get { return Enum.GetValues(typeof(WireFormat)).Cast<WireFormat>(); }
        }

        public IEnumerable<BlockSide> BlockSides
        {
            get { return BlockSide.Values; }
        }

        private FunctionBlock _viewedBlock;
        public FunctionBlock ViewedBlock
        {
            get
            {
                return _viewedBlock;
            }

            set
            {
                if (_viewedBlock != null)
                    _viewedBlock.BlockUpdate -= HandleBlockUpdate;

                _viewedBlock = value;

                if (value != null)
                    _viewedBlock.BlockUpdate += HandleBlockUpdate;

                OnPropertyChanged("ViewedBlock");
                OnViewedBlockChanged();
                SetControlTab();
            }
        }

        private ushort _inputValues;
        public ushort InputValues
        {
            get
            {
                return _inputValues;
            }

            set
            {
                if (value != _inputValues)
                {
                    _inputValues = value;
                    ViewedBlock.StartAsMain(value);
                    OnPropertyChanged("InputValues");
                }
            }
        }

        public ushort OutputValues
        {
            get
            {
                return ViewedBlock.GetValues(ViewedBlock.OutputSide);
            }
        }

        private Location _selectedLocation;
        public Location SelectedLocation
        {
            get
            {
                return _selectedLocation;
            }
            set
            {
                if(value != _selectedLocation)
                {
                    _selectedLocation = value;
                    SetControlTab();
                    OnPropertyChanged("SelectedLocation");
                }
            }
        }

        private Location _selectedAreaStart;
        public Location SelectedAreaStart
        {
            get
            {
                return _selectedAreaStart;
            }
            set
            {
                if(value != _selectedAreaStart)
                {
                    _selectedAreaStart = value;
                    SetControlTab();
                    OnPropertyChanged("SelectedAreaStart");
                }
            }
        }

        public int ControlTabIndex { get; set; }

        public BlockSide SelectionInputSide
        {
            get
            {
                ISingleInBlock block = ViewedBlock.GetBlockAt(SelectedLocation) as ISingleInBlock;
                return block == null ? null : block.InputSide;
            }
            set
            {
                ISingleInBlock block = ViewedBlock.GetBlockAt(SelectedLocation) as ISingleInBlock;
                if (block != null && value != null)
                {
                    ViewedBlock.RemoveBlock(SelectedLocation);
                    block.InputSide = value;
                    ViewedBlock.PlaceBlock(block as Block, SelectedLocation);
                    OnPropertyChanged("SelectionInputSide");
                    OnViewedBlockChanged();
                }
            }
        }

        public BlockSide SelectionOutputSide
        {
            get
            {
                ISingleOutBlock block = ViewedBlock.GetBlockAt(SelectedLocation) as ISingleOutBlock;
                return block == null ? null : block.OutputSide;
            }
            set
            {
                ISingleOutBlock block = ViewedBlock.GetBlockAt(SelectedLocation) as ISingleOutBlock;
                if (block != null && value != null)
                {
                    ViewedBlock.RemoveBlock(SelectedLocation);
                    block.OutputSide = value;
                    ViewedBlock.PlaceBlock(block as Block, SelectedLocation);
                    OnPropertyChanged("SelectionOutputSide");
                    OnViewedBlockChanged();
                }
            }
        }

        public WireFormat SelectionWireFormat
        {
            get
            {
                Wire wire = ViewedBlock.GetBlockAt(SelectedLocation) as Wire;
                return wire == null ? WireFormat.CROSS : wire.Format;
            }
            set
            {
                Wire wire = ViewedBlock.GetBlockAt(SelectedLocation) as Wire;
                if(wire != null)
                {
                    ViewedBlock.RemoveBlock(SelectedLocation);
                    wire.Format = value;
                    ViewedBlock.PlaceBlock(wire, SelectedLocation);
                    OnPropertyChanged("SelectionWireFormat");
                    OnViewedBlockChanged();
                }
            }
        }

        public bool SelectionInvert
        {
            get
            {
                DelayInverter inv = ViewedBlock.GetBlockAt(SelectedLocation) as DelayInverter;
                return inv != null && inv.Invert;
            }
            set
            {
                DelayInverter inv = ViewedBlock.GetBlockAt(SelectedLocation) as DelayInverter;
                if(inv != null)
                {
                    ViewedBlock.RemoveBlock(SelectedLocation);
                    inv.Invert = value;
                    ViewedBlock.PlaceBlock(inv, SelectedLocation);
                    OnPropertyChanged("SelectionInvert");
                    OnViewedBlockChanged();
                }
            }
        }

        public ushort SelectionMaskBits
        {
            get
            {
                Mask mask = ViewedBlock.GetBlockAt(SelectedLocation) as Mask;
                return mask == null ? (ushort)0 : mask.MaskBits;
            }
            set
            {
                Mask mask = ViewedBlock.GetBlockAt(SelectedLocation) as Mask;
                if (mask != null)
                {
                    ViewedBlock.RemoveBlock(SelectedLocation);
                    mask.MaskBits = value;
                    ViewedBlock.PlaceBlock(mask, SelectedLocation);
                    OnPropertyChanged("SelectionMaskBits");
                    OnViewedBlockChanged();
                }
            }
        }

        public int SelectionShiftAmount
        {
            get
            {
                Shifter shift = ViewedBlock.GetBlockAt(SelectedLocation) as Shifter;
                return shift == null ? 0 : shift.ShiftAmount;
            }
            set
            {
                Shifter shift = ViewedBlock.GetBlockAt(SelectedLocation) as Shifter;
                if (shift != null)
                {
                    ViewedBlock.RemoveBlock(SelectedLocation);
                    shift.ShiftAmount = value;
                    ViewedBlock.PlaceBlock(shift, SelectedLocation);
                    OnPropertyChanged("SelectionShiftAmount");
                    OnViewedBlockChanged();
                }
            }
        }

        public ushort SelectionValues
        {
            get
            {
                Block block = ViewedBlock.GetBlockAt(SelectedLocation);
                if(block is Wire)
                {
                    return ((Wire)block).GetValues();
                }
                else if(block is ISingleOutBlock)
                {
                    return block.GetValues(((ISingleOutBlock)block).OutputSide);
                }
                else
                {
                    return 0;
                }
            }
        }
        #endregion

        #region commands
        public ICommand TickCommand
        {
            get { return new DelegateCommand((x) => Tick()); }
        }

        public ICommand AddBlockCommand
        {
            get { return new DelegateCommand((s) => AddBlock(s)); }
        }

        public ICommand MoveIOBlockCommand
        {
            get { return new DelegateCommand((s) => MoveIOBlock(s)); }
        }

        public ICommand DeleteBlockCommand
        {
            get { return new DelegateCommand((x) => DeleteBlock()); }
        }

        public ICommand OpenFunctionBlockCommand
        {
            get { return new DelegateCommand((x) => OpenFunctionBlock()); }
        }

        public ICommand NewCommand
        {
            get { return new DelegateCommand((x) => New()); }
        }

        public ICommand SaveCommand
        {
            get { return new DelegateCommand((x) => Save()); }
        }

        public ICommand LoadCommand
        {
            get { return new DelegateCommand((x) => Load()); }
        }

        public ICommand CopyCommand
        {
            get { return new DelegateCommand((x) => Copy()); }
        }

        public ICommand PasteCommand
        {
            get { return new DelegateCommand((X) => Paste()); }
        }
        #endregion

        #region events
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        public event EventHandler ViewedBlockChanged;
        private void OnViewedBlockChanged()
        {
            ViewedBlockChanged?.Invoke(this, EventArgs.Empty);
        }
        #endregion

        public BlueTechViewModel()
        {
            ViewedBlock = new FunctionBlock();
        }

        private void HandleBlockUpdate(object sender, BlockUpdateEventArgs e)
        {
            OnPropertyChanged("OutputValues");
        }

        private void SetControlTab()
        {
            Block block = ViewedBlock.GetBlockAt(SelectedLocation);
            if (block == null)
                ControlTabIndex = 0;
            else if (block is Wire)
            {
                ControlTabIndex = 1;
                OnPropertyChanged("SelectionWireFormat");
                OnPropertyChanged("SelectionValues");
            }
            else if (block is DelayInverter)
            {
                ControlTabIndex = 2;
                OnPropertyChanged("SelectionInputSide");
                OnPropertyChanged("SelectionOutputSide");
                OnPropertyChanged("SelectionInvert");
            }
            else if (block is Mask)
            {
                ControlTabIndex = 3;
                OnPropertyChanged("SelectionInputSide");
                OnPropertyChanged("SelectionOutputSide");
                OnPropertyChanged("SelectionMaskBits");
            }
            else if (block is Shifter)
            {
                ControlTabIndex = 4;
                OnPropertyChanged("SelectionInputSide");
                OnPropertyChanged("SelectionOutputSide");
                OnPropertyChanged("SelectionShiftAmount");
            }
            else if (block is FunctionBlock)
            {
                ControlTabIndex = 5;
                OnPropertyChanged("SelectionInputSide");
                OnPropertyChanged("SelectionOutputSide");
            }
            else if (block is InputBlock)
            {
                ControlTabIndex = 6;
            }
            else if (block is OutputBlock)
            {
                ControlTabIndex = 7;
            }
            OnPropertyChanged("ControlTabIndex");
        }

        #region command implementations
        private void Tick()
        {
            ViewedBlock.Tick();
        }

        private void AddBlock(string blockType)
        {
            Block block = null;
            switch (blockType)
            {
                case "wire":
                    block = new Wire(WireFormat.CROSS);
                    break;
                case "delay":
                    block = new DelayInverter();
                    break;
                case "mask":
                    block = new Mask();
                    break;
                case "shift":
                    block = new Shifter();
                    break;
                case "func":
                    OpenFileDialog dlg = new OpenFileDialog();
                    dlg.DefaultExt = ".xml";
                    dlg.Filter = "XML Files|*.xml";
                    dlg.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + "\\Documents\\BlueTech\\";

                    bool? result = dlg.ShowDialog();

                    if (result.HasValue && (bool)result)
                        block = BlueTechGame.LoadFromXML(dlg.FileName);
                    
                    break;
            }
            if (block != null)
            {
                ViewedBlock.PlaceBlock(block, SelectedLocation);
                OnViewedBlockChanged();
                SetControlTab();
            }
        }

        private void MoveIOBlock(string block)
        {
            switch (block.ToLower())
            {
                case "input":
                    ViewedBlock.MoveInputBlockTo(SelectedLocation);
                    break;
                case "output":
                    ViewedBlock.MoveOutputBlockTo(SelectedLocation);
                    break;
            }
            OnViewedBlockChanged();
            SetControlTab();
        }

        private void DeleteBlock()
        {
            ViewedBlock.RemoveBlock(SelectedLocation);
            OnViewedBlockChanged();
            SetControlTab();
        }

        private void OpenFunctionBlock()
        {
            MainWindow window = new MainWindow((FunctionBlock)ViewedBlock.GetBlockAt(SelectedLocation));
            window.Show();
        }

        private void New()
        {
            ViewedBlock = new FunctionBlock();
            InputValues = 0;
            OnPropertyChanged("OutputValues");
        }

        private void Save()
        {
            SaveFileDialog dlg = new SaveFileDialog();
            dlg.DefaultExt = ".xml";
            dlg.Filter = "XML File|*.xml";
            dlg.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + "\\Documents\\BlueTech\\";

            bool? result = dlg.ShowDialog();

            if (result.HasValue && (bool)result)
            {
                BlueTechGame.SaveToXML(ViewedBlock, dlg.FileName);
            }
        }

        private void Load()
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.DefaultExt = ".xml";
            dlg.Filter = "XML Files|*.xml";
            dlg.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + "\\Documents\\BlueTech\\";

            bool? result = dlg.ShowDialog();

            if (result.HasValue && (bool)result)
            {
                try
                {
                    ViewedBlock = BlueTechGame.LoadFromXML(dlg.FileName);
                    InputValues = 0;
                    OnPropertyChanged("OutputValues");
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Exception Occured:\n" + ex.ToString(), "Error");
                }
            }
        }

        private void Copy()
        {
            int minX = Math.Min(SelectedLocation.X, SelectedAreaStart.X);
            int minY = Math.Min(SelectedLocation.Y, SelectedAreaStart.Y);
            int maxX = Math.Max(SelectedLocation.X, SelectedAreaStart.X);
            int maxY = Math.Max(SelectedLocation.Y, SelectedAreaStart.Y);

            clipboard.Clear();

            foreach (KeyValuePair<Location, Block> kvp in ViewedBlock.GetBlocks())
            {
                if (!(kvp.Value is InputBlock) && !(kvp.Value is OutputBlock) && kvp.Key.X >= minX && kvp.Key.X <= maxX && kvp.Key.Y >= minY && kvp.Key.Y <= maxY)
                {
                    int dx = kvp.Key.X - minX;
                    int dy = kvp.Key.Y - minY;
                    clipboard.Add(new Location(dx, dy), kvp.Value.Clone());
                }
            }
        }

        private void Paste()
        {
            int minX = Math.Min(SelectedLocation.X, SelectedAreaStart.X);
            int minY = Math.Min(SelectedLocation.Y, SelectedAreaStart.Y);

            foreach (KeyValuePair<Location, Block> kvp in clipboard)
            {
                int x = minX + kvp.Key.X;
                int y = minY + kvp.Key.Y;
                ViewedBlock.PlaceBlock(kvp.Value.Clone(), new Location(x, y));
            }

            SetControlTab();
            OnViewedBlockChanged();
        }
        #endregion
    }

    internal class DelegateCommand : ICommand
    {
        private readonly Action<string> action;

        public event EventHandler CanExecuteChanged;

        public DelegateCommand(Action<string> action)
        {
            this.action = action;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            action((string)parameter);
        }
    }
}
