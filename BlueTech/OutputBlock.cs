﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlueTech
{
    public class OutputBlock : Block, ISingleInBlock
    {
        private ushort values;

        public BlockSide InputSide { get; set; }

        public ushort GetValues()
        {
            return values;
        }

        public override void HandleBlockUpdate(object sender, BlockUpdateEventArgs e)
        {
            Block block = sender as Block;
            if(block != null)
            {
                ushort oldValues = values;
                values = block.GetValues(InputSide.Opposite);
                if (oldValues != values)
                    OnBlockUpdate(new BlockUpdateEventArgs());
            }
        }

        public override ushort GetValues(BlockSide side)
        {
            return values;
        }

        public override bool IsInput(BlockSide side)
        {
            return side == InputSide;
        }

        public override bool IsOutput(BlockSide side)
        {
            return false;
        }

        public override Block Clone()
        {
            return (Block)MemberwiseClone();
        }
    }
}
