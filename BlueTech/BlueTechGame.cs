﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace BlueTech
{
    public class BlueTechGame
    {
        private static int imgNum = 0;

        //static void Main(string[] args)
        //{
        //    FunctionBlock main = CreateFullAdder();

        //    MapFunctionBlock(main);

        //    for (int i = 0; i < 8; i++)
        //    {
        //        //ushort input = Convert.ToUInt16("0000 0000 0000 0000".Replace(" ", ""), 2);
        //        ushort input = (ushort)i;

        //        Console.WriteLine(Convert.ToString(input, 2).PadLeft(16, '0'));
        //        //Console.WriteLine("{0} + {1} + {2}", (input & 1) != 0, (input & 2) != 0, (input & 4) != 0);

        //        main.StartAsMain(input);
        //        for (int t = 0; t < 10; t++)
        //        {
        //            main.Tick();
        //        }

        //        Console.WriteLine(Convert.ToString(main.GetValues(main.OutputSide), 2).PadLeft(16, '0'));
        //        //.WriteLine("{0} (Carry {1})", main.GetValue(main.OutputSide, 0), main.GetValue(main.OutputSide, 2));
        //        Console.WriteLine();
        //    }
            

        //    Console.WriteLine("Finished. Press any key...");
        //    Console.ReadKey();
        //}

        private static void MapFunctionBlock(FunctionBlock func)
        {
            Bitmap img = new Bitmap(500, 500);
            int n = imgNum;
            imgNum++;
            using (Graphics gr = Graphics.FromImage(img))
            {
                int size = 40;
                int offsetX = -100;
                int offsetY = 200;

                gr.InterpolationMode = InterpolationMode.HighQualityBicubic;
                gr.SmoothingMode = SmoothingMode.HighQuality;
                gr.PixelOffsetMode = PixelOffsetMode.HighQuality;

                foreach (Location loc in func.GetBlocks().Keys)
                {
                    Block block = func.GetBlockAt(loc);
                    drawBlock(gr, block, img.Width / 2 + size * loc.X + offsetX, img.Height / 2 - size * loc.Y + offsetY, size);
                }
            }
            img.Save(func.Name + n + ".png");
        }

        private static void drawBlock(Graphics gr, Block block, int x, int y, int size)
        {
            int p1 = (int)(size * 0.1);
            int p2 = (int)(size * 0.2);
            int p4 = (int)(size * 0.4);

            if (block.GetType() == typeof(FunctionBlock))
            {
                gr.FillRectangle(Brushes.Gray, x, y, size, size);
                MapFunctionBlock((FunctionBlock)block);
            }
            else if (block.GetType() == typeof(Wire))
            {
                Wire wire = (Wire)block;
                gr.FillRectangle(Brushes.LightBlue, x + p4, y + p4, p2, p2);
                if(wire.IsInput(BlockSide.UP))
                    gr.FillRectangle(Brushes.LightBlue, x + p4, y, p2, p4);
                if (wire.IsInput(BlockSide.DOWN))
                    gr.FillRectangle(Brushes.LightBlue, x + p4, y + p4 + p2, p2, p4);
                if (wire.IsInput(BlockSide.LEFT))
                    gr.FillRectangle(Brushes.LightBlue, x, y + p4, p4, p2);
                if (wire.IsInput(BlockSide.RIGHT))
                    gr.FillRectangle(Brushes.LightBlue, x + p4 + p2, y + p4, p4, p2);
            }
            else if (block.GetType() == typeof(DelayInverter))
            {
                gr.FillRectangle(Brushes.DimGray, x, y, size, size);
                if (((DelayInverter)block).Invert)
                    gr.FillEllipse(Brushes.White, x + p4, y + p4, p2, p2);
                else
                    gr.DrawEllipse(new Pen(Color.White, 2), x + p4, y + p4, p2, p2);
            }
            else if (block.GetType() == typeof(Shifter))
            {
                gr.FillRectangle(Brushes.Blue, x, y, size, size);
            }
            else if (block.GetType() == typeof(Mask))
            {
                gr.FillRectangle(Brushes.CadetBlue, x, y, size, size);
            }
            else if (block.GetType() == typeof(InputBlock))
            {
                gr.FillRectangle(Brushes.LawnGreen, x, y, size, size);
            }
            else if (block.GetType() == typeof(OutputBlock))
            {
                gr.FillRectangle(Brushes.Red, x, y, size, size);
            }

            if (block.GetType() != typeof(Wire))
            {
                if (block.IsInput(BlockSide.UP))
                    gr.FillRectangle(Brushes.LawnGreen, x + p4, y + p1, p2, p2);
                else if (block.IsInput(BlockSide.DOWN))
                    gr.FillRectangle(Brushes.LawnGreen, x + p4, y + size - p1 - p2, p2, p2);
                else if (block.IsInput(BlockSide.LEFT))
                    gr.FillRectangle(Brushes.LawnGreen, x + p1, y + p4, p2, p2);
                else if (block.IsInput(BlockSide.RIGHT))
                    gr.FillRectangle(Brushes.LawnGreen, x + size - p1 - p2, y + p4, p2, p2);

                if (block.IsOutput(BlockSide.UP))
                    gr.FillRectangle(Brushes.Red, x + p4, y + p1, p2, p2);
                else if (block.IsOutput(BlockSide.DOWN))
                    gr.FillRectangle(Brushes.Red, x + p4, y + size - p1 - p2, p2, p2);
                else if (block.IsOutput(BlockSide.LEFT))
                    gr.FillRectangle(Brushes.Red, x + p1, y + p4, p2, p2);
                else if (block.IsOutput(BlockSide.RIGHT))
                    gr.FillRectangle(Brushes.Red, x + size - p1 - p2, y + p4, p2, p2);
            }
        }

        public static FunctionBlock LoadFromXML(string filename)
        {
            XElement root = XElement.Load(filename);
            return (FunctionBlock) ReadBlock(root);
        }

        private static Block ReadBlock(XElement element)
        {
            string name = element.Name.LocalName;
            switch (name)
            {
                case "FunctionBlock":
                    {
                        FunctionBlock block = new FunctionBlock();

                        string inputLoc = element.Attribute("InputBlock").Value;
                        string[] parts = inputLoc.Split(',');
                        block.MoveInputBlockTo(new Location(int.Parse(parts[0]), int.Parse(parts[1])));

                        string outputLoc = element.Attribute("OutputBlock").Value;
                        parts = outputLoc.Split(',');
                        block.MoveOutputBlockTo(new Location(int.Parse(parts[0]), int.Parse(parts[1])));

                    
                        XAttribute inAtt = element.Attribute("InputSide");
                        if (inAtt != null)
                            block.InputSide = BlockSideByName(inAtt.Value);

                        XAttribute outAtt = element.Attribute("OutputSide");
                        if (outAtt != null)
                            block.OutputSide = BlockSideByName(outAtt.Value);

                        XAttribute nameAtt = element.Attribute("Name");
                        if (nameAtt != null)
                            block.Name = nameAtt.Value;


                        foreach (XElement child in element.Elements())
                        {
                            int x = int.Parse(child.Attribute("X").Value);
                            int y = int.Parse(child.Attribute("Y").Value);
                            Block childBlock = ReadBlock(child);

                            if (childBlock != null)
                                block.PlaceBlock(childBlock, new Location(x, y));
                        }

                        return block;
                    }
                case "Wire":
                    {
                        string format = element.Attribute("WireFormat").Value;
                        Wire wire = new Wire(WireFormatByName(format));

                        return wire;
                    }
                case "DelayInverter":
                    {
                        DelayInverter block = new DelayInverter();

                        XAttribute inAtt = element.Attribute("InputSide");
                        if (inAtt != null)
                            block.InputSide = BlockSideByName(inAtt.Value);

                        XAttribute outAtt = element.Attribute("OutputSide");
                        if (outAtt != null)
                            block.OutputSide = BlockSideByName(outAtt.Value);

                        XAttribute invAtt = element.Attribute("Invert");
                        if (invAtt != null)
                            block.Invert = bool.Parse(invAtt.Value);

                        return block;
                    }
                case "Shifter":
                    {
                        Shifter block = new Shifter();

                        XAttribute inAtt = element.Attribute("InputSide");
                        if (inAtt != null)
                            block.InputSide = BlockSideByName(inAtt.Value);

                        XAttribute outAtt = element.Attribute("OutputSide");
                        if (outAtt != null)
                            block.OutputSide = BlockSideByName(outAtt.Value);

                        XAttribute shAtt = element.Attribute("ShiftAmount");
                        if (shAtt != null)
                            block.ShiftAmount = int.Parse(shAtt.Value);

                        return block;
                    }
                case "Mask":
                    {
                        Mask block = new Mask();

                        XAttribute inAtt = element.Attribute("InputSide");
                        if (inAtt != null)
                            block.InputSide = BlockSideByName(inAtt.Value);

                        XAttribute outAtt = element.Attribute("OutputSide");
                        if (outAtt != null)
                            block.OutputSide = BlockSideByName(outAtt.Value);

                        XAttribute shAtt = element.Attribute("MaskBits");
                        if (shAtt != null)
                            block.MaskBits = ushort.Parse(shAtt.Value);

                        return block;
                    }
                default:
                    return null;
            }
        }

        public static void SaveToXML(FunctionBlock func, string filename)
        {
            XElement root = WriteBlock(func);
            root.Save(filename);
        }

        private static XElement WriteBlock(Block block)
        {

            XElement elem = new XElement(block.GetType().Name);
            
            if (block.GetType() == typeof(FunctionBlock))
            {
                FunctionBlock func = (FunctionBlock)block;
                elem.SetAttributeValue("InputSide", func.InputSide.ToString());
                elem.SetAttributeValue("OutputSide", func.OutputSide.ToString());
                elem.SetAttributeValue("InputBlock", func.InputBlockLoc.ToString());
                elem.SetAttributeValue("OutputBlock", func.OutputBlockLoc.ToString());

                foreach(KeyValuePair<Location, Block> kvp in func.GetBlocks())
                {
                    if (kvp.Value.GetType() == typeof(InputBlock) || kvp.Value.GetType() == typeof(OutputBlock))
                        continue;

                    XElement childElem = WriteBlock(kvp.Value);
                    childElem.SetAttributeValue("X", kvp.Key.X);
                    childElem.SetAttributeValue("Y", kvp.Key.Y);
                    elem.Add(childElem);
                }
            }
            else if (block.GetType() == typeof(Wire))
            {
                elem.SetAttributeValue("WireFormat", ((Wire)block).Format);
            }
            else if (block.GetType() == typeof(DelayInverter))
            {
                DelayInverter inv = (DelayInverter)block;
                elem.SetAttributeValue("InputSide", inv.InputSide.ToString());
                elem.SetAttributeValue("OutputSide", inv.OutputSide.ToString());
                elem.SetAttributeValue("Invert", inv.Invert);
            }
            else if (block.GetType() == typeof(Shifter))
            {
                Shifter sh = (Shifter)block;
                elem.SetAttributeValue("InputSide", sh.InputSide.ToString());
                elem.SetAttributeValue("OutputSide", sh.OutputSide.ToString());
                elem.SetAttributeValue("ShiftAmount", sh.ShiftAmount);
            }
            else if (block.GetType() == typeof(Mask))
            {
                Mask mask = (Mask)block;
                elem.SetAttributeValue("InputSide", mask.InputSide.ToString());
                elem.SetAttributeValue("OutputSide", mask.OutputSide.ToString());
                elem.SetAttributeValue("MaskBits", mask.MaskBits);
            }

            return elem;
        }

        private static WireFormat WireFormatByName(string name)
        {
            switch (name.ToUpper())
            {
                case "HORIZONTAL":
                    return WireFormat.HORIZONTAL;
                case "VERTICAL":
                    return WireFormat.VERTICAL;
                case "TURN_UP_LEFT":
                    return WireFormat.TURN_UP_LEFT;
                case "TURN_UP_RIGHT":
                    return WireFormat.TURN_UP_RIGHT;
                case "TURN_DOWN_LEFT":
                    return WireFormat.TURN_DOWN_LEFT;
                case "TURN_DOWN_RIGHT":
                    return WireFormat.TURN_DOWN_RIGHT;
                case "T_UP":
                    return WireFormat.T_UP;
                case "T_DOWN":
                    return WireFormat.T_DOWN;
                case "T_LEFT":
                    return WireFormat.T_LEFT;
                case "T_RIGHT":
                    return WireFormat.T_RIGHT;
                default: case "CROSS":
                    return WireFormat.CROSS;
                case "BRIDGE":
                    return WireFormat.BRIDGE;
            }
        }

        private static BlockSide BlockSideByName(string name)
        {
            switch (name.ToUpper())
            {
                case "UP": return BlockSide.UP;
                case "DOWN": return BlockSide.DOWN;
                case "LEFT": return BlockSide.LEFT;
                case "RIGHT": return BlockSide.RIGHT;
                default: return null;
            }
        }
    }
}
