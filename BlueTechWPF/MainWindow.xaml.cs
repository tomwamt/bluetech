﻿using System;
using System.IO;
using System.Windows;

namespace BlueTech.Windows
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow() : this(null) { }
        public MainWindow(FunctionBlock block)
        {
            string[] files = Directory.GetFiles("Templates\\");
            string btPath = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + "\\Documents\\BlueTech\\";
            if (!Directory.Exists(btPath))
                Directory.CreateDirectory(btPath);
            foreach(string file in files)
            {
                string filename = Path.GetFileName(file);
                if(!File.Exists(btPath + filename))
                    File.Copy(file, btPath + filename);
            }

            InitializeComponent();

            ((BlueTechViewModel)DataContext).ViewedBlockChanged += GamePanel.HandleViewedBlockChanged;

            if (block != null)
            {
                ((BlueTechViewModel)DataContext).ViewedBlock = block;
            }
        }
    }
}
