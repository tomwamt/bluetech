﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlueTech
{
    public class Mask : Block, ISingleInBlock, ISingleOutBlock
    {
        private ushort values;

        public ushort MaskBits { get; set; } = ushort.MaxValue;
        public BlockSide InputSide { get; set; } = BlockSide.DOWN;
        public BlockSide OutputSide { get; set; } = BlockSide.UP;

        public override ushort GetValues(BlockSide side)
        {
            if (side == OutputSide) return (ushort)(values & MaskBits);
            else return 0;
        }

        public override void HandleBlockUpdate(object sender, BlockUpdateEventArgs e)
        {
            Block block = sender as Block;
            if(block != null)
            {
                ushort oldValues = values;
                values = block.GetValues(InputSide.Opposite);
                if (oldValues != values)
                    OnBlockUpdate();
            }
        }

        public override bool IsInput(BlockSide side)
        {
            return side == InputSide;
        }

        public override bool IsOutput(BlockSide side)
        {
            return side == OutputSide;
        }

        public override Block Clone()
        {
            return (Block)MemberwiseClone();
        }
    }
}
