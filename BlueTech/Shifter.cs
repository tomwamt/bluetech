﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlueTech
{
    public class Shifter : Block, ISingleInBlock, ISingleOutBlock
    {
        private ushort values;

        public BlockSide InputSide { get; set; } = BlockSide.DOWN;
        public BlockSide OutputSide { get; set; } = BlockSide.UP;
        public int ShiftAmount { get; set; } = 0;

        public override ushort GetValues(BlockSide side)
        {
            if (side == OutputSide)
                return values;
            else
                return 0;
        }

        public override void HandleBlockUpdate(object sender, BlockUpdateEventArgs e)
        {
            Block block = sender as Block;
            if(block != null)
            {
                ushort oldValues = values;
                if(ShiftAmount > 0)
                    values = (ushort)(block.GetValues(InputSide.Opposite) << ShiftAmount);
                else
                    values = (ushort)(block.GetValues(InputSide.Opposite) >> -ShiftAmount);

                if (values != oldValues)
                    OnBlockUpdate(new BlockUpdateEventArgs());
            }
        }

        public override bool IsInput(BlockSide side)
        {
            return side == InputSide;
        }

        public override bool IsOutput(BlockSide side)
        {
            return side == OutputSide;
        }

        public override Block Clone()
        {
            return (Block)MemberwiseClone();
        }
    }
}
