﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlueTech
{
    public class FunctionBlock : Block, ISingleInBlock, ISingleOutBlock
    {
        private Dictionary<Location, Block> blocks = new Dictionary<Location, Block>();
        
        public InputBlock Input { get; }
        public OutputBlock Output { get; }
        public BlockSide InputSide { get; set; }
        public BlockSide OutputSide { get; set; }
        public Location InputBlockLoc { get; private set; }
        public Location OutputBlockLoc { get; private set; }

        public string Name { get; set; }

        public FunctionBlock()
        {
            InputSide = BlockSide.DOWN;
            OutputSide = BlockSide.UP;

            Input = new InputBlock();
            Input.OutputSide = BlockSide.UP;
            InputBlockLoc = new Location(0, 0);
            PlaceBlock(Input, InputBlockLoc);

            Output = new OutputBlock();
            Output.InputSide = BlockSide.DOWN;
            Output.BlockUpdate += HandleOutputBlockUpdate;
            OutputBlockLoc = new Location(0, 1);
            PlaceBlock(Output, OutputBlockLoc);
        }

        public Block GetBlockAt(Location loc)
        {
            if (blocks.ContainsKey(loc))
                return blocks[loc];
            else
                return null;
        }

        public void PlaceBlock(Block block, Location loc)
        {
            if(GetBlockAt(loc) == null)
            {
                blocks[loc] = block;
                foreach(BlockSide side in BlockSide.Values)
                {
                    Block adj = GetBlockAt(loc.Adjacent(side));
                    if(adj != null)
                    {
                        if (block.IsOutput(side) && adj.IsInput(side.Opposite))
                        {
                            block.BlockUpdate += adj.HandleBlockUpdate;
                            adj.SetNeighbor(side.Opposite, block);
                        }
                        if(block.IsInput(side) && adj.IsOutput(side.Opposite))
                        { 
                            adj.BlockUpdate += block.HandleBlockUpdate;
                            block.SetNeighbor(side, adj);
                        }
                    }
                }
            }
        }

        public void RemoveBlock(Location loc)
        {
            Block block = blocks[loc];
            if (block == null || block == Input || block == Output) return;

            blocks.Remove(loc);
            DeregisterListeners(block, loc);
        }

        public void MoveInputBlockTo(Location loc)
        {
            blocks.Remove(InputBlockLoc);
            DeregisterListeners(Input, InputBlockLoc);

            InputBlockLoc = loc;
            PlaceBlock(Input, InputBlockLoc);
        }

        public void MoveOutputBlockTo(Location loc)
        {
            blocks.Remove(OutputBlockLoc);
            DeregisterListeners(Output, OutputBlockLoc);

            OutputBlockLoc = loc;
            PlaceBlock(Output, OutputBlockLoc);
        }

        private void DeregisterListeners(Block block, Location loc)
        {
            foreach (BlockSide side in BlockSide.Values)
            {
                Block adj = GetBlockAt(loc.Adjacent(side));
                if (adj != null)
                {
                    block.BlockUpdate -= adj.HandleBlockUpdate;
                    adj.SetNeighbor(side.Opposite, null);
                    adj.BlockUpdate -= block.HandleBlockUpdate;
                    block.SetNeighbor(side, null);
                }
            }
        }

        public void StartAsMain(ushort inValues)
        {
            Input.SetValues(inValues, true);
        }

        public override void Tick()
        {
            foreach(Block block in blocks.Values)
            {
                block.Tick();
            }
        }

        public ReadOnlyDictionary<Location, Block> GetBlocks()
        {
            return new ReadOnlyDictionary<Location, Block>(blocks);
        }

        public void HandleOutputBlockUpdate(object sender, BlockUpdateEventArgs e)
        {
            if (sender == Output)
                OnBlockUpdate(e);
        }

        public override void HandleBlockUpdate(object sender, BlockUpdateEventArgs e)
        {
            Block block = sender as Block;
            if(block != null)
            {
                Input.SetValues(block.GetValues(InputSide.Opposite));
            }
        }

        public override ushort GetValues(BlockSide side)
        {
            if (IsOutput(side))
                return Output.GetValues();
            else
                return 0;
        }

        public override bool IsInput(BlockSide side)
        {
            return side == InputSide;
        }

        public override bool IsOutput(BlockSide side)
        {
            return side == OutputSide;
        }

        public override Block Clone()
        {
            FunctionBlock block = new FunctionBlock();
            block.InputSide = InputSide;
            block.OutputSide = OutputSide;
            block.MoveOutputBlockTo(new Location(1000, 1000)); // Prevents bug where Input at 0,1 and Output at 0,0
            block.MoveInputBlockTo(InputBlockLoc);
            block.MoveOutputBlockTo(OutputBlockLoc);
            if(Name != null)
                block.Name = string.Copy(Name);

            foreach(KeyValuePair<Location, Block> kvp in blocks)
            {
                block.PlaceBlock(kvp.Value.Clone(), kvp.Key);
            }

            return block;
        }
    }
}
