﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlueTech
{
    public class Wire : Block
    {
        private ushort values;

        public WireFormat Format { get; set; }

        public Wire(WireFormat format)
        {
            Format = format;
        }

        private bool IsConnected(BlockSide side)
        {
            switch (Format)
            {
                case WireFormat.BRIDGE:
                case WireFormat.CROSS:
                    return true;
                case WireFormat.HORIZONTAL:
                    if      (side == BlockSide.UP)    return false;
                    else if (side == BlockSide.DOWN)  return false;
                    else if (side == BlockSide.LEFT)  return true;
                    else if (side == BlockSide.RIGHT) return true;
                    break;
                case WireFormat.VERTICAL:
                    if (side == BlockSide.UP) return true;
                    else if (side == BlockSide.DOWN) return true;
                    else if (side == BlockSide.LEFT) return false;
                    else if (side == BlockSide.RIGHT) return false;
                    break;
                case WireFormat.T_DOWN:
                    if (side == BlockSide.UP) return false;
                    else if (side == BlockSide.DOWN) return true;
                    else if (side == BlockSide.LEFT) return true;
                    else if (side == BlockSide.RIGHT) return true;
                    break;
                case WireFormat.T_LEFT:
                    if (side == BlockSide.UP) return true;
                    else if (side == BlockSide.DOWN) return true;
                    else if (side == BlockSide.LEFT) return true;
                    else if (side == BlockSide.RIGHT) return false;
                    break;
                case WireFormat.T_RIGHT:
                    if (side == BlockSide.UP) return true;
                    else if (side == BlockSide.DOWN) return true;
                    else if (side == BlockSide.LEFT) return false;
                    else if (side == BlockSide.RIGHT) return true;
                    break;
                case WireFormat.T_UP:
                    if (side == BlockSide.UP) return true;
                    else if (side == BlockSide.DOWN) return false;
                    else if (side == BlockSide.LEFT) return true;
                    else if (side == BlockSide.RIGHT) return true;
                    break;
                case WireFormat.TURN_DOWN_LEFT:
                    if (side == BlockSide.UP) return false;
                    else if (side == BlockSide.DOWN) return true;
                    else if (side == BlockSide.LEFT) return true;
                    else if (side == BlockSide.RIGHT) return false;
                    break;
                case WireFormat.TURN_DOWN_RIGHT:
                    if (side == BlockSide.UP) return false;
                    else if (side == BlockSide.DOWN) return true;
                    else if (side == BlockSide.LEFT) return false;
                    else if (side == BlockSide.RIGHT) return true;
                    break;
                case WireFormat.TURN_UP_LEFT:
                    if (side == BlockSide.UP) return true;
                    else if (side == BlockSide.DOWN) return false;
                    else if (side == BlockSide.LEFT) return true;
                    else if (side == BlockSide.RIGHT) return false;
                    break;
                case WireFormat.TURN_UP_RIGHT:
                    if (side == BlockSide.UP) return true;
                    else if (side == BlockSide.DOWN) return false;
                    else if (side == BlockSide.LEFT) return false;
                    else if (side == BlockSide.RIGHT) return true;
                    break;
            }
            return false;
        }

        public ushort GetValues()
        {
            return values;
        }

        public override ushort GetValues(BlockSide side)
        {
            if (IsConnected(side)) return values;
            else return 0;
        }

        public override void HandleBlockUpdate(object sender, BlockUpdateEventArgs e)
        {
            ushort oldValues = values;
            ushort newValues;
            if (sender.GetType() != typeof(Wire))
            {
                newValues = 0;
            }
            else
            {
                newValues = ((Wire)sender).GetValues();
            }

            foreach (BlockSide side in BlockSide.Values)
            {
                if (Neighbors.ContainsKey(side) && Neighbors[side].GetType() != typeof(Wire))
                    newValues = (ushort)(newValues | Neighbors[side].GetValues(side.Opposite));
            }
            values = newValues;

            if (oldValues != values) OnBlockUpdate(new BlockUpdateEventArgs());
            if(sender.GetType() == typeof(Wire) && ((Wire)sender).GetValues() != values) OnBlockUpdate(new BlockUpdateEventArgs());
        }

        public override bool IsInput(BlockSide side)
        {
            return IsConnected(side);
        }

        public override bool IsOutput(BlockSide side)
        {
            return IsConnected(side);
        }

        public override Block Clone()
        {
            return (Block)MemberwiseClone();
        }
    }

    public enum WireFormat
    {
        HORIZONTAL, VERTICAL, // --, |
        TURN_UP_LEFT, TURN_UP_RIGHT, TURN_DOWN_LEFT, TURN_DOWN_RIGHT, // _|, |_, ...
        T_UP, T_DOWN, T_LEFT, T_RIGHT, // _|_, T, -|, |-
        CROSS, BRIDGE // +, -|-
    }
}
