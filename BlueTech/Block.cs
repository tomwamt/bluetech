﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlueTech
{
    public abstract class Block
    {
        protected Dictionary<BlockSide, Block> Neighbors { get; } = new Dictionary<BlockSide, Block>();

        public event EventHandler<BlockUpdateEventArgs> BlockUpdate;

        public abstract bool IsInput(BlockSide side);
        public abstract bool IsOutput(BlockSide side);
        public abstract ushort GetValues(BlockSide side);
        public abstract void HandleBlockUpdate(object sender, BlockUpdateEventArgs e);
        public abstract Block Clone();

        public virtual void Tick() { }

        protected void OnBlockUpdate(BlockUpdateEventArgs e = null)
        {
            if (e == null)
                e = new BlockUpdateEventArgs();
            BlockUpdate?.Invoke(this, e);
        }

        public bool GetValue(BlockSide side, int wire)
        {
            return (GetValues(side) & (ushort)(1 << wire)) != 0;
        }

        public void SetNeighbor(BlockSide side, Block block)
        {
            if (block == null)
                Neighbors.Remove(side);
            else
                Neighbors[side] = block;
        }
    }

    public interface ISingleInBlock
    {
        BlockSide InputSide { get; set; }
    }

    public interface ISingleOutBlock
    {
        BlockSide OutputSide { get; set; }
    }

    public class BlockUpdateEventArgs : EventArgs
    {
        
    }

    public sealed class BlockSide
    {
        public static readonly BlockSide UP = new BlockSide();
        public static readonly BlockSide DOWN = new BlockSide();
        public static readonly BlockSide LEFT = new BlockSide();
        public static readonly BlockSide RIGHT = new BlockSide();

        public static readonly IReadOnlyCollection<BlockSide> Values = Array.AsReadOnly(
        new BlockSide[]{
            UP, DOWN, LEFT, RIGHT
        });

        static BlockSide()
        {
            UP.Opposite = DOWN;
            DOWN.Opposite = UP;
            LEFT.Opposite = RIGHT;
            RIGHT.Opposite = LEFT;
        }

        public BlockSide Opposite { get; private set; }

        public override string ToString()
        {
            if (this == UP) return "UP";
            else if (this == DOWN) return "DOWN";
            else if (this == LEFT) return "LEFT";
            else if (this == RIGHT) return "RIGHT";
            else return "What?";
        }
    }

    public struct Location
    {
        public int X { get; }
        public int Y { get; }

        public Location(int x, int y)
        {
            X = x;
            Y = y;
        }

        public Location Adjacent(BlockSide side)
        {
            if (side == BlockSide.UP) return new Location(X, Y + 1);
            else if (side == BlockSide.DOWN) return new Location(X, Y - 1);
            else if (side == BlockSide.LEFT) return new Location(X - 1, Y);
            else if (side == BlockSide.RIGHT) return new Location(X + 1, Y);
            return this;
        }

        public static bool operator==(Location left, Location right)
        {
            return left.Equals(right);
        }

        public static bool operator!=(Location left, Location right)
        {
            return !(left == right);
        }

        public override bool Equals(object obj)
        {
            if (GetType() != obj.GetType()) return false;

            Location other = (Location)obj;
            if (other.X != X) return false;
            if (other.Y != Y) return false;

            return true;
        }

        public override int GetHashCode()
        {
            return (X << 16) + (Y & 0x0000ffff);
        }

        public override string ToString()
        {
            return $"{X},{Y}";
        }
    }
}
